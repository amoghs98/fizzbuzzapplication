﻿namespace FizzBuzzApplication.Service.Tests
{
    using FizzBuzzApplication.Service.Interface;
    using FizzBuzzApplication.Service.Service;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class DivisibilityByFiveTests
    {
        private IDivisionRule divisibleByFive;
        private Mock<ICheckDay> mockcheckday;

        [SetUp]
        public void Initialise()
        {
            mockcheckday = new Mock<ICheckDay>();
            divisibleByFive = new DivisibilityByFive(mockcheckday.Object);
        }

        [TestCase(3, false)]
        [TestCase(5, true)]
        [TestCase(15, true)]
        public void IsDivisibleMethod_Tests(int input, bool expected)
        {
            // act
            var result = divisibleByFive.IsDivisible(input);

            // assert
            Assert.AreEqual(expected, result);
        }

        [TestCase(5, "buzz")]
        [TestCase(15, "buzz")]
        public void GetMessageMethod_Tests(int input, string expected)
        {
            // act
            var result = divisibleByFive.GetMessage();

                // assert
            Assert.AreEqual(expected, result);
        }
    }
}