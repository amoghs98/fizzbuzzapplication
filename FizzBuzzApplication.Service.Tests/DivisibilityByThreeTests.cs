﻿namespace FizzBuzzApplication.Service.Tests
{
    using FizzBuzzApplication.Service.Interface;
    using FizzBuzzApplication.Service.Service;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class DivisibilityByThreeTests
    {
        private IDivisionRule divisibleByThree;
        private Mock<ICheckDay> mockcheckday;

        [SetUp]
        public void Initialise()
        {
            mockcheckday = new Mock<ICheckDay>();
            divisibleByThree = new DivisibilityByThree(mockcheckday.Object);
        }

        [TestCase(3, true)]
        [TestCase(5, false)]
        [TestCase(15, true)]
        public void IsDivisibleMethod_Tests(int input, bool expected)
        {
            // act
            var result = divisibleByThree.IsDivisible(input);

            // assert
            Assert.AreEqual(expected, result);
        }

        [TestCase(3, "fizz")]
        [TestCase(15, "fizz")]
        public void GetMessageMethod_Tests(int input, string expected)
        {
            // act
            var result = divisibleByThree.GetMessage();

            // assert
            Assert.AreEqual(expected, result);
        }
    }
}
