﻿namespace FizzBuzzApplication.Service.Interface
{
    public interface IDivisionRule
    {
        bool IsDivisible(int input);

        string GetMessage();
    }
}