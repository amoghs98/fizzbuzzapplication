﻿namespace FizzBuzzApplication.Web.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using FizzBuzzApplication.Web.Helper;
    using PagedList;

    public class FizzBuzzModel
    {
        [Required(ErrorMessage = FizzBuzzWebConstants.RequiredErrorMessage)]
        [Display(Name = FizzBuzzWebConstants.DisplayMessage)]
        [Range(FizzBuzzWebConstants.LowerLimit, FizzBuzzWebConstants.UpperLimit, ErrorMessage = FizzBuzzWebConstants.RangeErrorMessage)]
        public int Input { get; set; }

        public IPagedList<string> FizzBuzzList { get; set; }
    }
}