﻿namespace FizzBuzzApplication.Service.Service
{
    using System.Collections.Generic;
    using System.Linq;
    using FizzBuzzApplication.Service.Interface;

    public class FizzBuzzService : IFizzBuzzService
    {
        private readonly IList<string> fizzbuuzList;
        private readonly IEnumerable<IDivisionRule> divisionByNumber;

        public FizzBuzzService(IEnumerable<IDivisionRule> divisionByNumber)
        {
            this.fizzbuuzList = new List<string>();
            this.divisionByNumber = divisionByNumber;
        }

        public IEnumerable<string> GetFizzBuzzList(int input)
        {
            for (int index = 1; index <= input; index++)
            {
                var isDivisible = this.divisionByNumber.Where(m => m.IsDivisible(index)).ToList();
                fizzbuuzList.Add(isDivisible.Any() ? string.Join(" ", isDivisible.Select(m => m.GetMessage())) : index.ToString());
            }

            return fizzbuuzList;
        }
    }
}