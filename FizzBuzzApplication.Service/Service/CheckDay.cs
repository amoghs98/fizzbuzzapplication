﻿namespace FizzBuzzApplication.Service.Service
{
    using System;
    using FizzBuzzApplication.Service.Interface;

    public class CheckDay : ICheckDay
    {
        private readonly DayOfWeek specifiedDay;

        public CheckDay(DayOfWeek specifiedDay)
        {
            this.specifiedDay = specifiedDay;
        }

        public bool SpecifiedDayCheck(DayOfWeek today)
        {
            return today.ToString() == specifiedDay.ToString();
        }
    }
}